+++
title = "Now"
date = "2024-11-10"
aliases = ["now"]
[ author ]
  name = "Silipwn"
+++
----
### Work/Research 

* Graduate student at Purdue CS.
* *Still* Fuzzing things
* Maybe some network stuff as well.

### Programming

* Looking at Python again :)

<!-- ###  Projects -->

<!-- * IN PROGRESS. -->

----

Was inspired by [Derek Sivers](https://sivers.org/now) to have *Now* page.

*Updated in Nov 2024 : From Indiana*
