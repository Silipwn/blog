+++
title = "Musicbox"
date = "2022-01-07"
aliases = ["music"]
[ author ]
  name = "Silipwn"
+++
----
### What is this?
Basically a small post to myself when I discover a new song. I think you end up storing the current state of mind, to the first time you hear the song.

{{< figure src="/jukebox.jpg" caption="(Figurative) Hoping to get one some day!" height="400" >}}

Don't believe me? Try it yourself!

- 2022-01-07 : [Dhoom by Euphoria featuring Shubha Mudgal](https://www.youtube.com/watch?v=nlqeTUGdMSs)<br>
  *credits to [Akul](https://akulpillai.com) and his youtube music suggestions :)*<br>
- 2022-01-28 : [イシュカン･コミュニケーション(Ishukan Communication) by Chorogonzu](https://music.youtube.com/watch?v=WhOesCYSnso)<br>
   *credits to [Kobayashi-san Chi no Maid](https://maid-dragon.fandom.com/wiki/Kobayashi-san_Chi_no_Maid_Dragon_Wiki)*<br>
- 2022-02-08 : [KLK by Arca & Rosalía](https://music.youtube.com/watch?v=RDzyyTHLQfo)<br>
   *Waiting for the Motomami album*<br>
- 2022-05-08 : [Point Blank from Stoto](https://music.youtube.com/watch?v=oh5U4LhSSEs)<br>
   *Stoto, I believe is quite underrated, very simple music*
- 2022-05-25 : [バイ・バイ少女時代 from Cherry by Tomo Sakurai](https://www.youtube.com/watch?v=-bRlSiIAWNc)<br>
    *Japanese Pop, is a different vibe!*
- 2022-06-04 : [Santé by Stromae](https://www.youtube.com/watch?v=P3QS83ubhHE)<br>
    *Stromae is a very unique artist, with a insane energy, check his [livestream](https://www.youtube.com/watch?v=eOZLDQm9c2E) for his album Racine Carrée*
- 2022-06-05 : [Honey Trap by Hozho](https://www.youtube.com/watch?v=iF-ucIgP0OE)<br>
    *Balance, a must if you like minimal techno and progressive music*
- 2022-06-16 : [Lollipop(Candyman) by Aqua](https://music.youtube.com/watch?v=UwZm4LZxeeI)<br>
    *Blast from the past; Aqua was on a different league*
- 2022-06-29 : [Não Tem Nada Não by Marcos Valle](https://open.spotify.com/track/4W3kBwsjFLcfa1Wm8qLijJ)<br>
    *Tropical vibes! Marcos Valle is regarded as a king in Brazilian pop*
- 2022-07-12 : [ステップバイステップ](https://www.youtube.com/watch?v=22VFPZVN3tQ&t=358s)<br>
    *Macroblank, a welcome change from what you usually hear in Vaporwave*
- 2022-07-25 : [Shakira](https://www.youtube.com/watch?v=CBVaz41ZN3Y)<br>
    *Hmm, not a song, but a interesting take on the old classic* (Discovered from [Shakira!](https://www.youtube.com/watch?v=AQ5ZRB9oWRw)) 
- 2022-11-12 : [Pine Walk Collection](https://www.mixcloud.com/pinewalkcollection/)<br>
    *Lot of Disco music, has a very rich history behind it [NYTimes Article](https://www.nytimes.com/interactive/2022/04/29/realestate/fire-island-pines-lost-soundtrack.html)*
- 2023-02-03 : [Agitations tropicales](https://www.youtube.com/watch?v=jda_k6r9R1A)<br> 
    *Wow, French music is just on a different level, magnifique!*
    - 2023-02-10 : [Peur des filles](https://youtu.be/VUH-z_SBlj0)<br>
    *Another banger!*
- 2023-02-25 : [Maharani by Karun feat. Arpit Bala, ReVo LEKHAK](https://www.youtube.com/watch?v=VhRJ-3T4Rcw)<br>
    *Indian Hip-Hop reaching new levels!*
- 2023-05-26 : [You & I by Dabeull feat Holybrune](https://www.youtube.com/watch?v=2e8x3fFZP3Q)<br>
    {{< youtube 2e8x3fFZP3Q>}}
    *Fonkiest songs, man 70-80s was a different vibe*
    *Check his [live](https://www.youtube.com/watch?v=Ik4DBIu8Igc) perfomance*
    {{< youtube Ik4DBIu8Igc>}}
- 2023-08-31 : [Vieux Farka Tourè et Khruangbin - Diarabi](https://www.youtube.com/watch?v=Ls7o_X4O6ho)<br>
    {{< youtube Ls7o_X4O6ho>}}
    *Khruangbin has been a unique sound all along, but it opened new doors with Vieux Farka Toure*
- 2024-01-27:  [Lover's Rock by TV Girl](https://www.youtube.com/watch?v=wcJY0WDe-H4)
    {{< youtube wcJY0WDe-H4>}}
    *Nice sample from an old R/B song, evokes an old nostalgic feeling*
- 2025-01-04: Minnum Palunkungal by KJ Yesudas from Johny Walker
    {{< youtube DESydmMq_IM>}}
    *Inspired from a reel/short seen on the social media for New Year*

<!-- - (date) : []()<br> -->
<!--     *credits*<br> -->

*Need more data? <br> Checkout my profile on [Listenbrainz](https://listenbrainz.org/user/silipwn/reports/)!*
