+++
title = "2016-2020 A quadrennial flashback?"
author = "Silipwn"
meta = true
date = 2021-01-01T00:00:00+05:30
lastmod = 2021-04-11T19:58:51+05:30
tags = ["reviews", "BTech"]
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Why a flashback?](#why-a-flashback)
- [Warning](#warning)
- [Why a BTech? And why Amrita?](#why-a-btech-and-why-amrita)
    - [Year 1](#year-1)
    - [Year 2](#year-2)
    - [Year 3](#year-3)
    - [Year 4](#year-4)
- [2020](#2020)

</div>
<!--endtoc-->


## Why a flashback? {#why-a-flashback}

I guess at some point you start walking down a path, and stuff happens so fast that you fail to look back on the path you have come by. So, here I am thinking about the stuff I've done, and where should I go now. ¯\\\_(ツ)\_/¯

I thought I have an idea about what to do next, I guess I'm just as confused as I was before deciding for a Bachelors in Technology. I guess you'll always wonder what the other option would have looked like, but again, I think there's no living in the past. So, let's try to look at stuff that happened, and what we can focus on.


## Warning {#warning}

> This really represents my personal experience and journey at Amrita Vishwa Vidhyapeetham, it may turn out to be the same for you or it entirely different. Sorry, that's how stuff turns out to be.


## Why a BTech? And why Amrita? {#why-a-btech-and-why-amrita}

I guess like every average Indian students who did PCM, the obvious choice is to look at IITs and the NITs, but yeah like most of them I also didn't make it. I think probably family and friends already knew this and made me apply to other private universities. Amrita Vishwa Vidhyapeetham was on the top of the list, mainly because a couple of cousins studied from Amrita, and are at a pretty good place right now. So, _I thought why not?_

And a couple of months after the exam, I got the results, which was a decent rank, and if luck favoured me, I could have a seat in one of the (then) 3 campuses. I went ahead for the counselling and decided that I'll go for Electronics and Communication Engineering, thinking that I'll get to play around with a lot of cool stuff[^fn:1], which was partially correct, but boy-oh-boy, I had a lot of other subjects that made me wonder,why I even bothered to get this degree, should have been happy with Computer Engineering.

I tried to jump across the branches as well, but my twisted fate, and the slow speed of bureaucracy made it extremely difficult. Here's where I think I began to see the real world, how getting things done (with strangers) is a complicated science. But, then I already gave up and thought what's the big deal, people still switch streams and work in the computer science field, _(Shouldn't be that hard)_

{{< figure src="/ox-hugo/amritapuri_beach.jpg" caption="Figure 1: The beach!" width="400" >}}

But I believe, that I got to experience a unique blend of teachers and subjects as well, not entirely sure if all of that will be useful later on, but some of it definitely helped.
Campus was picturesque being near a backwater and the ocean, which meant we could enjoy some greenery all around us, which is something that I dearly miss in the concrete jungle, I live in.

{{< figure src="/ox-hugo/amritapuri_greenery_rainbow.jpg" caption="Figure 2: The Greenery and a rainbow!" width="300" >}}


### Year 1 {#year-1}

In my first year, I was so sure of my decision, it seemed like a bad idea, cause the culture was quite different, since I've been living in a different state in childhood, I knew the local language, so I could interact with my classmates, but I guess something still felt a bit off.

I felt strange mainly because of an empty barrier, no it wasn't language, just perhaps still wondering what the f#$# is happening in life. Still, it took sometime before getting to know people.
There was this new felt time that we used to get, and I used to get bored quite often.
So, I thought I should learn something new and I thought a good way was to join a club. So there were mainly 3 clubs that you could enter.

1.  FoSS Club (now known as AmFOSS)[^fn:2]
2.  team bi0s (The CTF team)[It seemed to difficult to get in back then]
3.  IEEE Club

There were other non-technical clubs as well, but I don't really have a lot of non-technical skills, so I thought these would at least have something that I can understand and learn. I joined the FoSS Club hoping to learn programming (I only knew C and I really sucked at it). The aim was to join bi0s then, but I wasn't so sure if someone from an ECE background could join security. So, I stayed put and continued to learn about programming, Git and Linux.

And then couple of my fellow club members joined team bi0s, and I was interested in joining along them, we had a couple of introductory sessions for web security and forensics. I guess it's then that I realized that I was interested in pursuing security and joining team bi0s to learn more. The seniors, then suggested me to meet Vipin sir, the chief mentor for AmFOSS and bi0s. And after interacting with him, he helped me understand about hardware security and an interesting area that was developing in the security world.
I then started to attend sessions in club where we were taught basics of web security, reverse engineering, exploitation and forensics.

{{< figure src="/ox-hugo/bi0s2016.png" caption="Figure 3: The team back in 2016!" width="500" >}}

Academics was quite a breeze, mainly only introductory stuff in all topics except _Maths_, somehow solving stuff seemed like pointless to me.

Lot of co-circular activities, which was unusually high in Amritapuri history :D (Rest years were a different experience)
Speaking of the co-circular activities, due to my less frequent visits to my home (obvious reasons, costly to travel and not enough time), I made friends with a lot of people who came from a different part of the country, especially people from Eastern, Western and Northern India, like I mentioned the weekdays only had classes, and there was too much time to kill, weekends usually were spent spending time with these guys.


### Year 2/3 {#year-2-3}

Hmm, sophomore year, a lot happened during this time. We got promoted to the hostels on the other side of the backwaters, with much more _freedom_.

More or less, was getting started used to the college, finally remembered everyone's names (I had a terrible time doing that, I tend to forget names fast).

And technically things started to look more exciting, we started to play CTFs more seriously with the seniors, occasionally staying back at the lab to play CTFs at nights, which were awesome.

Started to look into reversing things, trying to learn about QEMU and Firmadyne and understanding how protocols like UART/I2C work.

On a secondary note, I also started wasting time to _rice_ my desktop, might have been unnecessary, but I guess I enjoyed tweaking my setup.

Academics started to get complicated, most of the foundational courses were being taught now, but to be honest, I was often distracted with random stuff, and there's something more cooler to do.

This year was quite dormant compared to the future, most of the time just went, I don't really remember how 


### Year 3 {#year-3}

This year was pretty intense, a lot of stuff kept happening around.

Here's when I started to look into specifics of learning IoT security, I got to go ESC hosted by CSAW at IITK, which was focusing create a covert channel on BLE. We didn't win, but got to learn a lot about the ideas from it.

{{< figure src="/ox-hugo/BLE_CSAW.jpg" caption="Figure 4: Presenting about BLE at IIT Kanpur" width="500" >}}

We started to expand the hardware team we had at bi0s. We started to look into more other areas of hardware security, like Radio, SCADA and automotive.

Also wanted to try some Electronics development stuff so, tried to learn about VLSI design and decided to participate in the Cadence Design Contest, learnt how to work with VHDL, understanding how things work in the designing industry. Managed to qualify for the finals and got to visit the Cadence office in Bengaluru.

Academics wise, things started to get pretty intense, as a person who didn't really care during the second year, I had a tough time co-relating things, which is fine, things would have been easier, if I had studied back then, now I just had to spend more time to understand stuff, and yeah I came to this conclusion, days before the exam so that was great.

We had started to train for campus placements and applications further, I had no clue what to do, I thought will decide next year, even though I thought I might not study more.


### Year 4 {#year-4}

The year started with an internship at Schneider, where I was working in the SCADA Security, understanding how companies include security and test of their devices before sending it into field.

This year was also interesting, we got a new room for the hardware team, lot of cool hardware as well. Team had grown reasonably big, not as big as bi0s though :P
Some of the cool hardware included a Proxmark RDV4, HackRF and a OSS 3-D printer.

The most interesting part was having a very cool project that for the InCTF 2019 edition, where we had designed a custom badge based on the design of Millennium Falcon (Yup, we had lot of Star Wars fanatics in the lab)

{{< figure src="https://i.ibb.co/2cJNDyW/photo-2020-01-21-10-42-35.jpg" caption="Figure 5: Our own Millennium Falcon!" width="500" >}}

And here is Season talking about the badge

{{< tweet user="InCTF" id="1210880930736291841" >}}

Academia wise _ze_ final year, 2019-20 right, that ended before 2020 began. Our semesters ended quite fast, in fact by Dec 2019, owing to the fact that lot of my batch mates had internships. I thought it will be good to indulge more in the lab, and managed to get an internship there, mainly working on learning side channel analysis and creating some cool projects for the lab.

For the next thing to do, I had also managed to secure an internship at IMDEA Software where I was planning to work on micro-architectural attacks on L1 caches.
So with this the 2019 year came to an end, which also marked an end of my academic classes. (I had 6 months to graduate, but had to work only on my project and internship)


## 2020 {#2020}

The year started off with a bang, 2020 was a supposedly a good year, that people had been looking forward too. But, I think that world had a different plan.

{{< figure src="/ox-hugo/Nullcon_2020.jpg" caption="Figure 6: At Nullcon 2020" width="500" link="/ox-hugo/Nullcon_2020.jpg" >}}

We managed to win the Hardware CTF at Nullcon, one of the few hardware CTFs in India. Learned a lot of new things in the CTF, it's like you learn at 10x speed, even though that means sacrificing a lot of foundational knowledge, I think it's better to learn at a higher speed, and then take some time and reflect back on what you have learnt, don't agree?[^fn:3]

That was the best event that happened that year, after that the entire year got remote. (_which was good and bad at the same time_)

Well this has been hell of a year, with a disease that spread too fast for the world to control, and a lockdown that had a time-in-limbo effect, I still don't think a lot of time has been past, usually when a year passes, you look back and see lot as happened. At least it feels like lot has happened, this year, it wasn't exactly like that.

So after, I completed by projects at college and work at the lab, I started my internship at IMDEA remotely (>:(, wanted to always go to Madrid), none the less, learnt a lot about caches, processors and research in general.

Staying at home with my grandparents, meant a lot of time for learning gardening and roaming around the house, so I had been learning bit about plants in general, developed lot of respect for the people who take up agriculture as a full time profession.

Fast-forward to now, I think a lot of stuff happened,a lot of lessons learnt but just like Sisyphus, all of are just destined to keep pushing the boulder again and again, which means I'll probably keep repeating those mistakes.

But I am extremely thankful for people, I have met during the journey, whether they stood with or against me, I guess they might have somehow helped me improve. (Probably the against ones wouldn't have, but yeah end on a good note, right?)

[^fn:1]: Hardware I meant xD
[^fn:2]: <https://amfoss.in>
[^fn:3]: _Comment, or send me mail. I'd love a discussion._

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
