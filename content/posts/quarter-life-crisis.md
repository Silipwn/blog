+++
title = "Quarter Life Crises?"
url = "/thisisnotaflag/quarter-life-crises.html"
author = ["Silipwn"]
date = 2024-05-11T12:34:09Z
lastmod = 2024-07-06T06:45:46
tags = ["life", "misc"]
[_build]
  list = 'never'     # If 'always' it will appear on /posts/ and in sitemap
[cascade._build]
  list = 'local'     # Nicely done!
+++
# DRAFT Under REVIEW 
*(Thank you for taking reviewing this!)*

Yes, actually *crises*, because it's not just one crisis. Everyday maybe a new crisis.

Mostly a reflection or like a snapshot of my life at this point, it is some sense a letter to future me, or unless you are being forced to read this.

Had been avoiding to write this for a long time, but now I guess is the best time to write it. Was having a discussion yesterday, you should try to write atleast 30 minutes a day.
Though that was for research papers to make progress, but that's a different story for now, can't get too deviated.

So as an effort to get my own model to generate some text for reflection, maybe you'd enjoy reading this.
I hope it's better than GPT or Claude today, or maybe they are better, on ironically you are just gonna feed this to that, and extract a summary, please don't do that ideally.
It's kinda slow to write in this age of AI slop, maybe this is what you'd probably prefer reading.

I wonder how people end up writing everyday, 500 words or so? Insane now that I think about it?
I have been stuck at writing this for ever, summer is kinda of slow for an academic as you have less engagements.
*(here I thought you could live an ivory tower away from the real world)*

So in an effort to get back to the real world, thought of doing something more useful
I remember reading somewhere about producing than consuming or maybe create more than you consume.

So here we were trying to create something new, *useful*.

Everyone has heard about the Midlife crisis, when you reach 40, but what about when you reach 25?
Maybe it's not really quarter but more of a third of your life is done
Then you start really rethinking your life

You start thinking about all the things you have planned to do but never done?
You also start seeing other people move on the next phase in life, settling down, getting into relationships/marriages, and here I'm still figuring out what to make for dinner (I think that's probably gonna plague me for decades to come)
There is a new found exploration to learn about things that you often would have ignored

In my case:
* More physical activities as was stuck inside indoors mentally for the past 10 years and maybe physically for 2 years for Covid.
* Also religion and philosophy, as started to notice few things don't really have answers to or make sense right now.
* Eating healthier or atleast trying to.
* Spending too much time on Instagram seeing people getting married.
* Also bunch of regret about things?
* Maybe life would have been easier the other way.
  You've started to see the forks that exist down the road
  Some of them now seem like some paths everyone takes

In general figuring out what to do with life
The usual flow is the same you (atleast in India) maybe for a middle class family (Pew research about Middle Class) (Also a very famous scene from Banglore Days)
1. Finish school with good marks
2. Get into a good college
3. Get into a good company
4. Get married
5. Have kids
6. Start from 1

But is that what we really want?
	Low key want to figure out what we really need in life

Few tenets that I now try to keep
* Never mess with sleep or food
		Then can derail you quite easily
* There's always other portions of life
		Some sadness, some happiness
* Stuff can always go bad, be prepared?
* Social Media is a very difficult topic in general
		Can be too broad, but important theme is that do you need it more that it needs you?
