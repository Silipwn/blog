+++
title = "AVR RE #1"
meta = true
toc = true
description = "AVR Reversing Series"
type = ["posts","post"]
categories = ["AVR","RE"]
date = "2020-04-02"
author = "Silipwn"
+++
# AVR Reversing Series #1
### Work In Progress
TLDR; Short guide to get you started with AVR Development + Reversing

Well, I wanted to write this for a long time, I was worried that it might not be
complete, well let it be that way then. :)

These series will be focusing exclusively on getting a setup for debugging AVR
programs. I started my journey with microcontrollers learning things through
Arduino and its framework. It is very convinient to code using the framework and
the IDE, but it often removes a lot of debugging and extra info.

Like any other task, in order to learn how to break or take something apart, we
must also learn build it. Hence, I'll just everyone to try out the above
tutorial, if it seems too complicated, you can compile a example program for
Arduino IDE. 

## Stuff to install: 
#### Arch Packages
```sudo pacman -S arduino arduino-avr-core avr-gdb```

**NOTE**: arduino-avr-core will install avr-gcc/avr-libc/avrdude (Compiling and Flashing)

#### Ubuntu 
[Guide](https://ubuntu.com/tutorials/install-the-arduino-ide-overview)

## Things to learn
### What's AVR? 
It's a family of microcontrollers mainly known for:
* Modified-Harvard architecture
* Applications in Embedded Devices
* Use in open hardware devices(like Arduino)

[Wikipedia](https://en.wikipedia.org/wiki/AVR_microcontrollers) tells a lot about the history as well, give it a read!


### How to create AVR binaries? 
So, I thought of trying out writing bare-metal programs for microcontrollers. I
understand, there's not much difference between using the Arduino Framework and
writing a embedded C program for the Arduino. Arduino libraries are often
wrappers or rewrites around the libc functions. Mika Tuppola has written an
excellent blog on using embedded C for Arduino.[[0]](https://appelsiini.net/2011/simple-usart-with-avr-libc/)

If you are using the Arduino IDE to compile your programs, please check your
```/tmp``` directory in GNU/Linux. And for Windows please check the Temp folder.
And if you are OK with verbose output during compilation and flashing, enable
```show verbose output``` in the ```Preferences``` window of the IDE. It allows
you get more info, about the compilation and flashing process.
 
Let's take a simple program to being compiling with.
