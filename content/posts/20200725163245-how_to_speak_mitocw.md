+++
title = "How to Speak - MITOCW"
author = ["Silipwn"]
date = 2020-07-25T16:00:00+05:30
lastmod = 2020-07-26T16:19:05+05:30
tags = ["reading", "notes"]
categories = ["notes"]
draft = false
+++

-source :: <https://www.youtube.com/watch?v=Unzc731iCUY>

-info :: How to Speak by Patrick Winston


## Literal Notes {#literal-notes}

1 hr video about speaking professionaly

Anecdote : Story about going battle without weapon

Success determined by the ability

-   Quality -> Knowledge, practice, inherent talent
    Decreasing order

Anecdote : mary lou gymnast


### Starting a talk {#starting-a-talk}

-   No jokes, instead use promises to deliver by end of the talk.
-   Cycle around the ideas (Thrice atleast)
-   Build fence (_Differentiate between already existing work_)
-   Give numeric stats and agenda about the talk
-   Ask a question to get people back from fogging out


### Time Place {#time-place}

-   `11 AM` best time in an academic setting, everyone is awake and no chances of sleeping
-   `Well lit` places to enable max concentration, dim light places can force people to sleep.
-   `Cased`, speaker should ideally visit the place and get familizarized before
-   `Populated` (_Not sure why?_)


### Board {#board}

-   Gives a `graphical` advantage ; easier to connect and communicate
-   `Speed` is easier to comprehend (_compared to slides_)
-   Helps link with mirror neurons allegedly, gives a feeling of being connected to the mechanism


### Props {#props}

-   Anecdote : Play with a stove showing the state of the play.
-   Helps understanding stuff with a better perspective
-   Example : Cycle tire, mechanics, right hand rule, duct to reduce problem size


### Slides {#slides}

-   Never too many words
-   Stay near the slides
-   Images should be simple
-   Simple plain background
-   Avoid logos and title (_I guess that could sometimes not be an option, maybe stick to things on Title Page_)
-   People tend to focus on the slide
-   Print on paper to check and verify if it is good
-   Avoid laser pointer and use pointers in the slide itself
-   `Hapax Legomenon`&nbsp;[^fn:1] _A central idea for the entire presentation_


### Meta {#meta}

-   Example : Map coloring algorithm without and with optimazation (Similar to the promise point)
-   Oral Exams : Bring friends and tell them to make you cry
-   Job Talks : Have a Problem and Approach to solve ; should be able to explain to some one in ~5 minutes
-   Become Famous
    -   `You can never get used to becoming ignored`
    -   Need lot of 'S' words:
        -   Symbol
        -   Slogan
        -   Salient
        -   Surprise
        -   Story


### End {#end}

-   Mention collaboraters in the beginning
-   Never end with FAQ/Questions
-   No Thank You and The End
-   Maybe end with a joke, _gives them a feeling that they had fun during the talk_
-   Shake hands with the conference org `¯\_(ツ)_/¯`

[^fn:1]: (m: that only occurs once in the context)
