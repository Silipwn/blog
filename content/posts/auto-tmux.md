+++
title = "Auto Tmux"
author = ["Silipwn"]
date = 2022-01-01
lastmod = 2022-01-01T12:03:51-05:00
tags = ["tmux", "fish"]
categories = ["notes"]
+++

## Auto Tmux config {#auto-tmux-config}

I recently read an [article](https://www.markhansen.co.nz/auto-start-tmux/) for auto-starting tmux on login with `fish` (yeah auto-completion is good enough to lose POSIX compatibility).

Well I thought what if I can expand it to make it a bit interactive to use.

Ideally it should ask me,

1.  if I want to have non-tmux session, mainly if it's for a small task that would take <~ 15 minutes.
2.  if I want to create a new session, for something based on a new work/topic/idea.
3.  if I want to attach some existing session, that's already on the machine.

What better way to spend to holidays, I thought. And I spent some time trying to adapt the code from the article to work to my bidding.

Code from the article:

```bash
if status is-interactive
    # Adapted from https://github.com/fish-shell/fish-shell/issues/4434#issuecomment-332626369
    # only run in interactive (not automated SSH for example)
    if status is-interactive
    # don't nest inside another tmux
    and not set -q TMUX
    # Adapted from https://unix.stackexchange.com/a/176885/347104
    # Create session 'main' or attach to 'main' if already exists.
    tmux new-session -A -s main
end
```

Slightly modified version:

```bash
if status is-interactive
    # https://www.markhansen.co.nz/auto-start-tmux/
    if not set -q TMUX
        read -l -P 'Attach to tmux [Name/l/m] ?' confirm
        switch $confirm
            # Scenario where I want to list all existing sessions
            case l L
                tmux attach-session -t (tmux list-sessions -F '#{session_name}' | fzf)
            # A scenario I want to left alone (Just hitting an extra return finishes this)
            # TODO: Someday understand how this exactly works
            case ''
                echo "As you wish, sir"
            # A scenario for the general tmux session
            case m main Main
                tmux new-session -A -s main
            # If you have a custom name for a session to create
            case '*'
                tmux new-session -A -s $confirm
    end
end
```

Should ideally have the latest dotfiles at [Github](https://github.com/Silipwn/Dots)
