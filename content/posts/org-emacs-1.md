+++
title = "Using Emacs and org-mode (Part 1)"
author = "Silipwn"
date = 2020-07-18T16:17:00+05:30
lastmod = 2020-09-28T08:40:44+05:30
tags = ["emacs", "org"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Really why, Emacs?](#really-why-emacs)
    - [How's Org-Mode for good?](#how-s-org-mode-for-good)
    - [How does it manage your day](#how-does-it-manage-your-day)
    - [Creating Notes](#creating-notes)
- [Org-Agenda](#org-agenda)
- [Org-Roam](#org-roam)
    - [Org-Roam-Bibtex + Org-ref + Org-noter](#org-roam-bibtex-plus-org-ref-plus-org-noter)
- [Notmuch](#notmuch)

</div>
<!--endtoc-->


## Really why, Emacs? {#really-why-emacs}

Well mainly cause, `I really look inside what I'm using xD` (not really). It's for the superior Org-Mode. Markdown on steroids, someone might kill me
for saying this. But yeah it's much more than what markdown can do for you.

If you think that's too much work to set up, please do yourself a favour and install [VSCode.](https://code.visualstudio.com/)
Or if that's too \`non-configurable\` for you, maybe try Vim, it does one job in a very efficient way.

And my setup, it's `emacs+vim` and currently using Doom emacs to manage the configuration.

This is intended to be a living document, so the things might keep changing often. And more or less, something new comes so, there's always a series of
experiments going on with Org-mode and Emacs.

Better performance compared to VSCode (Drinks less RAM :P)[^fn:1].

Before you delve any deeper, let me warn you about the depths of the rabbit hole.
`Mandatory warning image:`

{{< figure src="/ox-hugo/real_programmers.png" >}}


### How's Org-Mode for good? {#how-s-org-mode-for-good}

-   Export to almost all types of files
    -   LaTeX
    -   HTML
    -   Good 'ol ascii text
    -   Markdown
    -   Github Flavored Markdown (with help of Pandoc[^fn:2])
    -   Even docx (Again with help of Pandoc[^fn:2])
-   Inbuilt `agenda` to track meetings/appointments
-   `org-clock` to manage time spent on working on various projects.
-   And I'm sure there are lots other reasons but not sure, if I know them all.


### How does it manage your day {#how-does-it-manage-your-day}

-   `Org-Agenda` is a \`scheduler\` that allows you look at your schedule for the day.

{{< figure src="/ox-hugo/Org-Mode-1-Agenda.png" >}}

-   Custom notification script adapted from various sources, notifies me about the meetings or deadlines, I have to meet.
-   `org-clock` allows you the ability to clock in to various tasks and track how much time you have spent on things.

    ```org
      :LOGBOOK:
      CLOCK: [2020-07-08 Wed 15:47]--[2020-07-08 Wed 15:57] =>  0:10
      CLOCK: [2020-06-23 Tue 11:56]--[2020-06-23 Tue 13:02] =>  1:06
      CLOCK: [2020-06-14 Sun 20:43]--[2020-06-14 Sun 20:44] =>  0:01
      :END:
    ```

    This is the time, I've been spending on this task :P


### Creating Notes {#creating-notes}

I guess this comes a big thing to me because I never used to make documentation or keep notes for the things I have done. More or less, I have tried to make note taking as effortless as possible. And Org-Mode allows me to do that :)

Some purists may disagree that the below packages are not only for notes, but I guess, that's how I see it.

-   Plain Old Notebook

    There's a lot of ways to manage notes, in Org-Mode.
    You could always try the simple method of writing notes into a single file which I used to, when I started to use Doom emacs. The below is a snippet, from `Notebook.org`, I used to have, I don't use it much though.

    ```org
      ** Custom Fortunes
    ​  + =some people buy chicks some do TLD - mindfreeze, on a fine day=
    ​  + =Friends don’t let friends use heroin or org-mode—are you ever really going to make back the time it takes to learn & customize org-mode?=
      ** Ricing perks
      *** Starship
      It requires ~noto-fonts-emoji~ to work as well
    ```
-   Org-Roam

    Currently, using this special section for this.
-   Org-Brain

    Had chosen between this and org-roam, and wasn't disappointed. Might check on it someday.
-   Custom tool that works for you?

    I think of it someday, was inspired by [Karl Voit's blog on the same](https://karl-voit.at/2020/06/14/Zettelkasten-concerns/).


## Org-Agenda {#org-agenda}

This acts as the standard for almost all things in scheduling, I usually refer this to schedule, my day. Check if there are any deadlines or meetings for the day. It also helps in tracking a couple of habits I'm trying to build, a couple of them being touch typing and journal-ing.

{{< figure src="/ox-hugo/Org-Mode-1-Habit.png" >}}

-   You can notice the  \* in different colours, the blue color indicates a done task while green is that I started didn't complete and red that I didn't do it at all.


## Org-Roam {#org-roam}

This particular package deserves a special mention as they early on started developing this package as an `open-source` & `free` alternative to Roam Research and having the concept of interlinked notes, is definitely pretty cool. A lot of new features are being talked about and thought of being added, which is pretty awesome.
I usually combine this with `org-roam-server` package which allows creating pretty visualizations among my notes. For e.g.

{{< figure src="/ox-hugo/Org-Mode-1-Roam.png" >}}

It allows me to track a lot of notes, (TBH my notes look pretty bad, I have seen some better visualizations)


### Org-Roam-Bibtex + Org-ref + Org-noter {#org-roam-bibtex-plus-org-ref-plus-org-noter}

I have also started to manage the papers I read with Org-roam, usually maintain a main bibtex file, which is can then allow me to read my papers in emacs itself and create notes on the basis of them, most of the inspiration comes from the [Youtube Video](https://www.youtube.com/watch?v=Wy9WvF5gWYg)  by Zaeph, his dotfiles are also worth reading. Much of org-noter config I use is based on his [dotfiles](https://github.com/zaeph/.emacs.d).


## Notmuch {#notmuch}

Well, I also read my mail in emacs, I usually pull all my using `isync` and then tag them using `notmuch`, which works flawlessly with emacs. So, I can compose and respond to mail using emacs itself. :)

_TLDR; If you are used to using `vim` and are looking for a full fledged alternative, I'm sure `emacs` with `evil` will cover almost all of your use cases._

[^fn:1]: Based on personal experience, Electron drinks RAM
[^fn:2]: <https://pandoc.org/>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
