+++
title = "Effective tools for computer systems research"
author = ["Silipwn"]
date = 2020-06-11
lastmod = 2020-07-11T19:40:12+05:30
tags = ["reading", "notes"]
categories = ["notes"]
draft = false
+++

Source
: <https://nymity.ch/book/#organise-your-reading>

[Note:Reading]({{< relref "20200501193128-reading" >}})
<span class="timestamp-wrapper"><span class="timestamp">[2020-06-11 Thu 11:40]</span></span>


## Quotes {#quotes}

-   `Before diving into a paper, know what you want to get out of it.`
-   `Ideally, an abstract reveals the problem that the paper tries to solve, explains how it solves the problem, and what the results are. A well-written abstract provides enough information for you to decide if the paper is worth diving into.`
-   `Adopt the mindset that you are reviewing rather than reading a paper.`
-   `Your null hypothesis should be to distrust a paper’s results and only through sound reasoning and rigorous methods can a paper change your mind.`


## Things to think about {#things-to-think-about}

> -   Briefly summarise the paper’s method.
>
>     --  Is the method section detailed enough to facilitate reproduction?
>
> -   What are the paper’s assumptions?
>
>     --  How robust and realistic are these assumptions?
>     --  Can you think of counter-examples to these assumptions?
>
> -   What are the paper’s key results?
>
>     --  Do these results contradict or confirm existing work?
>
> -   How could the paper (its method, presentation, or results) be improved?
>
> -   What follow-up research questions come to mind?
> -   What’s the paper’s conclusion?
>
>     --  Do the results support the conclusions


## Summary {#summary}

-   No necessity to read a paper from cover to cover, I guess, unless specifically insisted by Profs :O
-   (Trial run in lab?) Maybe a reading group, to increase reading papers from specific areas
-   `Maintaining a Bibtex file for research`, currently in progress, let's see how it turns out.
-   `paraphrasing` to read through the introduction,conclusion to get a better idea, then see if it makes, focus on review than reading a textbook, accept the fact
    that the author can be wrong, see if possible to replicate the results, keep checking Scholar alerts/Arxiv not sure about that, well scihub and mailing the author for work :)
