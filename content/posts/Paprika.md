---
title: "Paprika"
author: "Silipwn"
date: 2020-02-21 
type: reviews
images:
tags:
  - Anime
  - Psychological
  - Sci-Fi
---

### Paprika (2006) 

Direction:[Satoshi Kon](https://en.wikipedia.org/wiki/Satoshi_Kon)

[IMDB](https://www.imdb.com/title/tt0851578/)


### Rating
### ***** 5/5

### Summary

The movie is one of the greatest works by [Satoshi
Kon](https://en.wikipedia.org/wiki/Satoshi_Kon), this remains to be one of the
most awesome psychological thrillers, I've ever seen. 

The story is based on the novel of the same name, by [Yasutaka
Tsutsui](https://en.wikipedia.org/wiki/Yasutaka_Tsutsui) the story revolves around
the creation of new form of psycho-therapy, known as *dream therapy*. A device 'DC Mini', is made
to facilitate this therapy allowing its users to share their dreams, together.
The head of the team designing DC Mini , Dr. Atsuko Chiba, uses
the machine illegally to help people outside the facility assuming an alter-ego
of *'Paprika'*. The inventor of the DC Mini, Kōsaku Tokita, works with Atsuko,
in the same facility. 

Things start going haywire, when one of the DC Mini is stolen from the lab. The
device being in prototype state still needs to be configured to restrict people
from entering other's dreams. 

Not trying to spoil much of the story, I'll write to like about the things that
I liked in the movie. The movie revolves nicely around the concept of duality
and I believe that to be one of the things that Satoshi Kon aims for since it seems to be
pretty obvious from his other notable work, like [Perfect
Blue](https://en.wikipedia.org/wiki/Perfect_Blue). The way how the camera plays
with the reality is something that would be extremely difficult to do with the
live-action sequences which is probably one of the reasons, Satoshi Kon stuck with
animation. The movie does a really great work of interpreting the collision of
the real world and the dream world, I guess the word on the grapevine is the
fact Christopher Nolan was inspired from this movie, while creating
*[Inception](https://en.wikipedia.org/wiki/Inception)*. The way Satoshi Kon
plays with the concept of space and time is commendable. It seems to be a single flow of
transition, with the viewer having no idea how everything started.The
transitions seem to seem just in place, without the user ever noticing the
difference in the scene. He also uses this to show the difference between the
lives people show and the lives people live, allowing you to completely
understand the character. 


The soundtrack is again absolutely fascinating work done by [Susumu
Hirasawa](https://en.wikipedia.org/wiki/Susumu_Hirasawa), and I'm currently listening to
*"The Girl in Byakkoya - White Tiger Field" (白虎野の娘)* and *"Parade" (パレー
ド)*. Susumu Hirasawa rose to fame after working with Satoshi Kon and
developing the soundtrack for
[Berserk](https://en.wikipedia.org/wiki/Berserk_(manga)). Being highly
experimental with his compositions, his score brings out the best of animation
and the story line. 

I thought of listing my favourite scenes but it seems the list just keeps
growing to long. 

### Trailer 
And here is the [link](https://www.youtube.com/watch?v=SBrUhQ0_qYA).

Or watch it here if you like: 
{{< youtube  SBrUhQ0_qYA >}}
