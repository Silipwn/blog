+++
title = "Self Hosting with Raspberry Pi 4 (Part 1)"
author = "Silipwn"
date = 2020-10-17T13:17:00+05:30
lastmod = 2020-10-17T14:16:26+05:30
tags = ["rpi", "selfhost"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Self Hosting 101](#self-hosting-101)
- [Hardware required?](#hardware-required)
- [Things I Installed?](#things-i-installed)

</div>
<!--endtoc-->


## Self Hosting 101 {#self-hosting-101}

This lockdown I managed to get a Raspberry Pi 4 (RPi) for me, it was a bucket list item to have a secondary Linux machine, to spin up containers and run necessary services 24x7. (And to avoid buying expensive hardware). Well, definitely RPi fits the bill, and consumes less power as well.


### List of things required in for the server {#list-of-things-required-in-for-the-server}

-   Should be able to run a lot of programs :P
-   Simple Media server.
-   Network wide Ad-blocker (Almost a necessity these days).
-   Music Streaming (MPD, just love it too much)
-   Secondary VPN client for connecting to my VPN.


## Hardware required? {#hardware-required}

-   Raspberry Pi 4
-   A HDD for storage. (SSD would be awesome if you have the budget)
-   Fans cause RPi4 has a bad reputation for heating up, and the heat dissipation not being that good in the board.
-   A proper 3A Capable USB C Cable (recommended)
    (Side note: turns out the length of the cable is equally important&nbsp;[^fn:1])


## Things I Installed? {#things-i-installed}


### MPD {#mpd}

The best music player I ever encountered, combined with `ncmpcpp`, it allows me to remote control my songs on Raspberry Pi.


### Pi-Hole Ad-blocker {#pi-hole-ad-blocker}

Slightly annoying on the docker install, so defaulting to normal install. Can't run nginx/apache now on port 80


### Docker {#docker}

Well the fastest thing to get something deployed, pretty useful for testing things out.

You can always `docker rmi x` :)


### Wireguard VPN Client {#wireguard-vpn-client}

:\\ (Well not everything is accessible everywhere)


### Media Server {#media-server}

Share all types of files easily and instantly.

I tried using Jellyfin which felt feature rich but too much for my needs, I ended up browsing for some good simple file servers.

-   [GoHTTPServer](https://github.com/codeskyblue/gohttpserver), seemed like a good choice for use at home for sharing things, but the release was quite old, and it looked slightly heavy.
-   [Gossa](https://github.com/pldubouilh/gossa), was the best choice I felt, only required Go and was simple enough for the features I need.

Thought about running a Samba,NFS or WebDAV, but lot of things to configure, a single binary with CLI flags is what I was interested in.

That's about it for now, maybe later on I get sometime to do more stuff and write about it.

[^fn:1]: [Raspberry Pi and the dreaded undervoltage notifications](http://jon.netdork.net/2019/02/08/raspberry-pi-and-the-dreaded-undervoltage-notifications/)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
