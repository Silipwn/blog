+++
title = "My Setup"
type = ["posts","post"]
tags = [
    "rice",
    "setup",
]
date = "2019-09-25"
lastmod = "2024-11-10"
finished = "wip"
categories = [
    "Ricing",
    "Linux",
]
author = "Silipwn"
+++
## Hardware
### Laptop
The laptop that I use in an [Dell Inspiron
7560](https://www.dell.com/en-in/shop/laptops-2-in-1-pcs/inspiron-15-7560/spd/inspiron-15-7560-laptop)
with some nice bezel ^-^. It is a 15'' laptop with i7, 8 gigs of RAM, 256
gigs of SSD and 1 tera HDD, also a NVIDIA Graphics Card (Probably 940MX)
(Absolutely P.I.T.A to use, happy with Intel for now).It is a bit heavy machine,
but has been absolutely wonderful to use (ignoring minor hiccups).

Update (09-01-2022): Now have switched to [Apple M1 Macbook Air](https://www.apple.com/shop/buy-mac/macbook-air), strangely was the only laptop with good perfomance in that price range,
the hardware is certainly good and I certainly didn't want to carry a heavy laptop around. (Not a big fan of the walled ecosystem, but yeah people like [Cypher](https://matrix.fandom.com/wiki/Cypher) exist)

Update (11-10-2024): Now switched to a ThinkPad T480s, wanted some light laptop that can be used around everywhere. Snagged a good deal on eBay. M1 was too good :)

### Smartphone
My phone is again customized with [Arrow-OS](https://arrowos.net/), device is
`whyred` (Redmi Note 5 Pro). But I use it a lot, maybe would head back to a
dumb-phone?

Update (09-01-2022): Switched to iPhone 12. (No comments here)

Update (11-10-2024): Switched to a Pixel, cause iPhone sucks!

### Headphones
I recently got a pair of [JBL](https://in.jbl.com/JBL+E55BT.html), pretty good
noise cancellation (passive).

Update (09-01-2022): Switched to a Philips SHP500 HiFi Precision over-ear the headphones. 

Update (11-10-2024): Switched to earphones mostly, with a good pair for foam tips!

## Software 
Distros I have tried:
   
    * Ubuntu (Everyone's first time)
    * Debian
    * Manjaro
    * Arch

I made a switch to the Arch side, about a year ago. Learnt a lot in the process,
recently tried Ubuntu, but couldn't resist going back to Arch. So, back to arch,
but this time for a change I've installed a hybrid-setup using i3-gaps and XFCE,
so, I could easily configure a lot of stuff, connecting to the headphones would
have been such a headache. And just i3-gaps makes your system a bit dull as
well, maybe because I had lot of issuing in Firefox and other applications. 
Dotfiles are currently a mess, might put them online sometime, not sure when though.
I spend most of my time, in the browser, terminal or `mpv`. 

Update (09-01-2022): Since Linux is still in the process of being ported to M1, using OSX as a daily driver. It's not as great as Linux but defintely better than Windows.

Update (11-10-2024): Linux is the best! 2024 is the year of the Linux desktop! (But now only use Debian + default config)

#### Terminal
`termite`

Update (09-01-2022): Now using `kitty` exclusively on Linux and `iterm2` on OSX.

Update (11-10-2024): `kitty` as been good enough, never bothered to switch :)

#### Shell 
Vanilla `fish` + `tmux`

Update (09-01-2022): Still fish with couple of plugins and `fisher` for managing them.

Update (11-10-2024): `bash` + `tmux` + `atuin` and sometimes `starship`

#### Editor
I used to be a `vim`user, but I have switched to the other side of `emacs`,but I still am on the
dark side, currently using evil-mode with
[doom-emacs](https://github.com/hlissner/doom-emacs). 

Reasons to switch: 
    
    * Org Mode
    * MELPA Packages (You could possible do everything in Emacs itself)
    * Self-Documented

Update (09-01-2022): Switched back to vim, emacs was getting a bit too much :) Still use org-mode a lot

Update (11-10-2024): Lazyvim is good enough!

#### Browser

Firefox with `vim-vixen` and `umatrix`

Update (11-10-2024): I think this kinda of keeps switching have both the browsers installed anyway.

#### WM+DE+etc

    * DE: XFCE
    * WM: i3-gaps
    * Status-bar: bumblee-status
    * Application Launcher: rofi
    * Window Compositor: Compton

Update (11-10-2024): Gnome 3 + Extensions
