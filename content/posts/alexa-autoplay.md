+++
title = "Disable autoplay in Amazon Alexa in Linux"
author = ["Silipwn"]
lastmod = 2020-12-24T10:39:36+05:30
tags = ["alexa", "notes"]
categories = ["notes"]
draft = false
+++

## What was the issue? {#what-was-the-issue}

I got an Amazon Alexa as a gift, and it was lying around (no one found it interesting at home). I heard that you could use it as a bluetooth speaker, which seemed like a good thing to me. (Laptop's speakers have started to show it's age). The weird issue was Alexa would trying to keep on playing stuff, even after I paused it. I believe that the MPRIS protocol[^fn:1] and the Bluetooth Remote Control policies, allowed the device to control the media on it's own. So, even if I pause something on my laptop, it would continue playing after a while, which is terrible.


## How did I fix this? {#how-did-i-fix-this}

-   [X] ~~Tried to put Alexa in microphone off mode (wondering that maybe that is what triggers the autoplay)~~
-   [X] ~~Tried to play around with `bluetoothctl` to disable the relevant control UUIDs (Gave up; was getting too complicated)~~
-   Tried a solution from the wiki[^fn:2], since I didn't really require to control my media (via the device itself), I went ahead and disabled `avrcp`, which managed to stop the weird behaviour.

So, the **solution is to disable the `avrcp` plugin** in the `bluetooth.service`

1.  Edit the `bluetooth.service` with `sudo systemctl edit --full bluetooth.service`
2.  Change the `ExecStart=/usr/lib/bluetooth/bluetoothd` to `ExecStart=/usr/lib/bluetooth/bluetoothd --noplugin=avrcp`
3.  Restart the service!

    That should be it!
    If it doesn't work out, feel free to contact me and let's try to get to the bottom of this?

[^fn:1]: <https://www.freedesktop.org/wiki/Specifications/mpris-spec/>
[^fn:2]: `https://wiki.archlinux.org/index.php/Bluetooth_headset#Apple_AirPods_have_low_volume`
