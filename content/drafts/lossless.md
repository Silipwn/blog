+++
title = "Lossless Audio: Is it worth the time?"
author = ["Silipwn"]
date = 2023-02-18T09:34:09
lastmod = 2023-02-18T09:34:09
tags = ["music", "audio"]
weight = 2001
+++
# DRAFT

As usual, I found a new hobby to *waste* time on. 

*If only if I could get back to work, sigh!*

## Why audio though?
I still remember the first time, I listened to music with earphones(I think it was a family friend's iPod nano), the song was Shakira's Hips Don't Lie. It was quite an experience, after spending almost ~12 years listening to songs on speakers, the first time I heard the song, it was as if I discovered a completely new song. And I was hooked! I soon got a Casette player(It was a Phillips Casette Player, not Sony :P) as a gift. But didn't really collect a lot of cassetes. But everything started there


