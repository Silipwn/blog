+++
title = "Open Source vs Proprietary [Software]"
author = ["Silipwn"]
date = 2022-05-08T13:03:55
lastmod = 2022-06-08T13:03:55
tags = ["software", "misc"]
weight = 2001
+++
# DRAFT
# Why this discussion? 
I think every other day one has to make a decision in order to select which software/tool/package to use for some task. And generally I used to Google, and select the tool that used to work. When I started to find for tools, it was usually the one that works. 

Then, I discovered about alternative applications which offered more flexibility and freedom when it came to your software. I started looking at open source software in general. 

> I am assuming 2 reasons:
>
> a. People generally allow lot of customizations to the software you are using, and you can always modify stuff 
>
> b. You don't have to really pay anything for the software [Makes sense as a student, where you save money for more important stuff (like food, duh?)]

But with some more years, it was good to have a convenient software with customizations, but with those software you would have a learning curve involved. Sometimes there's time, usually there's none.

So, you then come into this dilemma whether to take the first software you see or take the that's open source, maintained and can be used? 
Let's first define what Open Source Software (OSS) and proprietary software (PS) mean.
### What is OSS? 
Open-Source Software is defined as 
> computer software that is released under a license in which the copyright holder grants users the rights to use, study, change, and distribute the software and its source code to anyone and for any purpose.

from the wiki[^1].
### Why is it useful?
0. You don't have to pay to use. (Yeah, you can't deny this!)
1. You have the source code public, which allows anyone to modify/edit or add new features 
on top of your code. 
> Why would this be that useful?
+ Because releasing code in open, allows people to add new features, report bugs. 
+ And the biggest benefit being that if one day the developer, stops working on that, someone can always continue working from there!

*I assume, a lot of people reading this would be using some part of software that was developed as Open Source (Chromium/Firefox/Edge/Safari)*
### Okay, why isn't everything OSS then?
> Hmm, everthing isn't free, yet! No free lunch, isn't it?

Now of course, I'm not an expert, but here's my 2c:

0. It's possible that the code is something that's crucial for the company's business. Or there's legal issues that don't allow the code to be public.
1. Maintainaince is a big issue. Most of the OSS is volunteer driven, often unpaid, so sometimes people find time to work on projects, sometimes they won't.

### What is proprietary software?
Alternative to Open Source, usually comes at a cost. But you have an assurance of full technical support. 

[^1]: https://en.wikipedia.org/wiki/Open-source_software
