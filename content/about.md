+++
title = "About"
date = "2021-11-23T10:10:14-0500"
aliases = ["about-us","contact"]
[ author ]
  name = "Silipwn"
+++

<h3>About Page</h3>

I am Ashwin, a grad student at [Purdue University](https://www.cs.purdue.edu/) working at the [Pursec](https://pursec.cs.purdue.edu/) lab.

{{< figure src="/whoami.jpg" caption="Trying to find reasons to smile like that!" width="250" >}}


I did my under-grad at Amrita Vishwa Vidhyapeetham and was a member of [teambi0s](https://bi0s.in) mainly looking into hardware and RE stuff, usually breaking things pretty frequently.

Wondering what I do [now](../now) ?

You like music? I do too. Check out my [favourites](../music)!

### Want to talk? 

#### Mail: `contact<at the rate>as-hw.in`

### Site

The site used to use a fork of rhadzon's [theme](https://github.com/rhazdon/hugo-theme-hello-friend-ng).
Now, it just uses [no style,please](https://github.com/Masellum/hugo-theme-nostyleplease). Why?[^1]

Some of the content is in markdown, and some in Org-Mode, which is converted to Markdown using kaushalmodi's [ox-hugo](https://github.com/kaushalmodi/ox-hugo), and the plain text is available on my [Gitlab](https://gitlab.com/Silipwn/blog). 

The font used is [Alegreya](https://www.huertatipografica.com/en/fonts/alegreya-ht-pro) inspired from what [krasjet](https://krasjet.com/) has.

#### Privacy
I use [GoatCounter](https://github.com/arp242/goatcounter) to track people who visit the main page of the blog. 

[^1]: M I N I M A L
