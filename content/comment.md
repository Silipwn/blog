+++
title = "Comments"
aliases = ["comments"]
[ author ]
  name = "Silipwn"
+++
----
### How does this work?
Simple, you send a mail to `~silipwn/comments <at> lists.sr.ht` , a public mailing list hosted at [sourcehut](https://lists.sr.ht/~silipwn/comments).

Ideally, follow the mailing list [etiquette](https://man.sr.ht/lists.sr.ht/etiquette.md) 

_Use plain text_ :)
