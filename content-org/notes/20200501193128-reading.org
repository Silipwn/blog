#+hugo_base_dir: ./../..
#+hugo_auto_set_lastmod: t
#+hugo_weight: auto
#+hugo_section: notes
#+author: Silipwn
#+date: 2020-06-11
#+hugo_tags: reading notes
#+hugo_categories: notes
#+bibliography: ~/Dropbox/references.bib
#+TITLE: Reading
* Papers
Wondering why to read papers after all
** Articles
[[http://michaelrbernste.in/2014/10/21/should-i-read-papers.html][Should I Read Papers]]
- Not that great, is more philosophical type *meh*
[[https://violentmetaphors.com/2013/08/25/how-to-read-and-understand-a-scientific-paper-2/][Read Paper from a non scientist perspective]]
- Has a PDF on the same link
- Seems more in depth, explaining the entire issue
- Write stuff you don't understand
- Check about the journal you're reading from
[[https://web.stanford.edu/class/ee384m/Handouts/HowtoReadPaper.pdf][How to Read A Paper - S Keshav]]

3 pass approach

1st Pass
- Read Title,Abstract,Intro,Conculsion
- Headings
- Refs
2nd Pass
- Understand the figures/diagrams ; Check labels etc
- Ignore proofs etc
- Check for papers yet to read
3rd pass
- Understand in depth -> should be able to create a gist from memory, should be able to explain stuff to others.

[[https://www.eecs.harvard.edu/~michaelm/postscripts/ReadPaper.pdf][Read Paper - Michael M]]

=When you read a research paper,  your goal is to understand  the scientific contributions the authors aremaking.This is not an easy task.It may require going over the paper several times.  Expect to spend several hoursto read a paper=
- Critical about the paper, assumptions are right, logical reasoning, gathered data etc.
- Creative about the further extensions, ideas etc.
- Create notes as you read. (Helpful later on)
- A summary after first reading. (1 paragraph)
- Compare to other works if possible
Ideally maybe create a one page review/note containing:
- A one or two sentence summary of the paper.
- A deeper,  more extensive  outline  of  the main  points  of  the paper,  including for example  assumptions made, arguments presented, data analyzed, and conclusions drawn.
- Any limitations or extensions you see for the ideas in the paper.
- Your opinion of the paper; primarily, the quality of the ideas and its potential impact
[[https://www.cs.jhu.edu/~jason/advice/how-to-read-a-paper.html][Read a Techincal Paper - Jason Eisner]]
- Multi pass reading similar to S Keshav
  Give limited time per page.
- Write as you read (keeps attention more focused?)
- Annotate the PDF directly (pdf-tools to the rescue/Xournal as well) [Low-Level Notes]
- Paraphrasing the content, should be able to distill the content, summarize things that are interesting and record ideas for work?
- Don't waste time adding images, refs, only the understood part ;)
- Organize notes properly? org-ref for me I guess
- Set aside time for reading maybe 30-40 minutes, start reading where you find time
- Breadth-first exploration
+ read a lot of abstracts (and skim the papers as needed) before deciding which papers are best to read
+ it's okay to read multiple related papers at once, flipping back and forth so that they clarify one another
+ to get a feel for the research landscape in an area, flip through the proceedings of a relevant recent workshop, conference, or special-theme journal issue

- When the going gets tough, switch to background reading
+ textbooks or tutorials
+ review articles
+ introductions and lit review chapters from dissertations
+ early papers that are heavily cited
+ sometimes Wikipedia
=Ask yourself dumb questions and answer them!=

** Websites
[[file:websites/effective_tools_for_computer_systems_research.org][Note:Effective tools for computer systems research]]


Could be helpful if you don't have access.  https://sci-hub.now.sh/ -> Find Sci-Hub

Or can use [[https://openaccessbutton.org/][OpenAccess]].
** Probable Approach
Create a multiple passes for reading paper
1: Skim read : Intro/Conclusion/Heading
2: Understand figures/graphs/attack: 1 paragraph summary: main outline
3: Deep Read: Write notes and create review note ; opinion/review
* Philosophy
** Websites
*** [[http://philosophizethis.org/i-want-to-read-more-about-philosophy-where-do-i-start/][Where Do I start? PhilosophizeThis]]
=Don't start with source texts!=
+ Staying away from original sources for a year or so, the point it probably to get used to the background and time of that age. Re-translate and interperted in a different way.\\
=it’s crucial you understand a TON about a lot of auxiliary stuff that may seem to have little to do with what was actually being written about. Things like=
#+BEGIN_QUOTE
What questions were being asked in philosophy at the time?

What were the specific connotations of the words used at the time?

What did the author THINK those connotations were?

What questions did the author think were worth answering?

Where did the author get their information? Was it accurate?

What major historical events were going on? What minor, highly specific events were going on locally?

What was the author’s personality like?
#+END_QUOTE
+ Better to start with generic work in the modern times to get a better understanding of the work.
#+BEGIN_QUOTE
A Brief History of Thought by Luc Ferry

The Consolations of Philosophy by Alain de Botton
#+END_QUOTE
Giving these a read
*** [[https://medium.com/the-apeiron-blog/how-to-read-philosophy-and-other-difficult-books-80da559f2124][Aperion Blog Medium]]
+ Read secondary texts, to get a better idea about the idea of the author.
+ Philosopher's Toolkit read about analytic terms etc, ~The Philosopher's Toolkit by Baggini~ should be a good start.
+ Don't delve into details, keep an eye on the bigger picture
+ Open to changing interpreions and changing beliefs
+ Taking things slow
